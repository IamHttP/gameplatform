/**
 * Function used for getting all shapes hit from a single click (not from a selection box)
 */
declare function getShapesFromClick(shapes: any, layerName: any, x: any, y: any): any[];
export default getShapesFromClick;
