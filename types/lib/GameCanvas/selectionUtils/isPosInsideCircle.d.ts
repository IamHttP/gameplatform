/**
 * Utility function to detect if a point is inside a circle
 * @param x
 * @param y
 * @param centerX
 * @param centerY
 * @param radius
 * @return {boolean}
 */
declare function isPosInsideCircle(x: any, y: any, centerX: any, centerY: any, radius: any): boolean;
export default isPosInsideCircle;
